from os import listdir, path, replace
import re
import subprocess
from datetime import timedelta

from .Image import Image
from .Music import Music
from .Size import Size


def get_media_duration(file_path) -> timedelta:
    try:
        duration = re.findall('[0-9][0-9]:[0-9][0-9]:[0-9][0-9]', subprocess.run(['ffprobe', '-i', file_path], capture_output=True, text=True).stderr)[0].split(':')
        return timedelta(hours=int(duration[0]), minutes=int(duration[1]), seconds=int(duration[2]))
    except IndexError:
        return  None

class MediaManager:
    VIDEO_FILE_TMP = "video_tmp.mp4"
    AUDIO_FILE_TMP = "audio_tmp.mp3"
    SLIDESHOW_FILE = "final.mp4"
    AUDIO_TYPE = {".mp3"}
    IMAGE_TYPE = {".png", ".jpeg", ".jpg"}

    def __init__(self, dir_path: str):
        self.dir_path = dir_path
        self.images = MediaManager.get_images_from_directory(dir_path)
        self.audios = MediaManager.get_audios_from_directory(dir_path)

    @staticmethod
    def get_audios_from_directory(dir_path: str):
        audios = []
        for file in listdir(dir_path):
            _, ext = path.splitext(file)
            if ext in MediaManager.AUDIO_TYPE:
                file_path = str(path.abspath(file))
                audios.append(Music(file_path))
        return audios

    @staticmethod
    def get_images_from_directory(dir_path: str):
        images = []
        for file in listdir(dir_path):
            _, ext = path.splitext(file)
            if ext in MediaManager.IMAGE_TYPE:
                file_path = str(path.abspath(file))
                image_duration = 3
                image_size = Size(1280, 720)
                images.append(Image(file_path, image_duration, image_size))
        return images

    def join_audio_tracks(self):
        ffmpeg_command = ["ffmpeg"]
        for audio in self.audios:
            ffmpeg_command += ['-i', audio.path]

        ffmpeg_command.append('-filter_complex')
        filter_complex = ""
        for index, audio in enumerate(self.audios):
            filter_complex += "[{index}:a]{fade_out}[a{index}];".format(index=index, fade_out=audio.fade_out)
        for index in range(len(self.audios)):
            filter_complex += "[a{}]".format(index)
        filter_complex += "concat=n={}:v=0:a=1[a]".format(len(self.audios))
        ffmpeg_command.append(filter_complex)
        ffmpeg_command.append('-map')
        ffmpeg_command.append('[a]')
        ffmpeg_command.append(self.AUDIO_FILE_TMP)

        subprocess.run(ffmpeg_command)
        #print(ffmpeg_command)

    def join_photo_in_video(self):
        ffmpeg_command = ['ffmpeg']
        for image in self.images:
            ffmpeg_command += image.loop_parameter.split()
        ffmpeg_command.append('-filter_complex')
        filter_complex = ""
        for index, image in enumerate(self.images):
            if index == 0:
                filter_complex += "[{}:v]{},{},{},{}[v{}];".format(index, image.scale, image.pad, image.setsar, image.fade_out, index)
            else:
                filter_complex += "[{}:v]{},{},{},{},{}[v{}];".format(index, image.scale, image.pad, image.setsar, image.fade_in, image.fade_out, index)
        for index in range(len(self.images)):
            filter_complex += "[v{}]".format(index)
        filter_complex += "concat=n={}:v=1:a=0,format=yuv420p[v]".format(len(self.images))
        ffmpeg_command.append(filter_complex)
        ffmpeg_command.append('-map')
        ffmpeg_command.append('[v]')
        ffmpeg_command.append(self.VIDEO_FILE_TMP)

        subprocess.run(ffmpeg_command)
        #print(ffmpeg_command)

    def cut_audio(self, video_duration: int):
        subprocess.run(['ffmpeg', '-y', '-i', self.AUDIO_FILE_TMP, '-ss', '0', '-to', str(int(video_duration)+2), '-c', 'copy', "tmp_" + self.AUDIO_FILE_TMP])
        replace('tmp_' + self.AUDIO_FILE_TMP, self.AUDIO_FILE_TMP)

    def cut_audio_if_longer_than_video(self) -> tuple:
        audio_duration = get_media_duration(self.AUDIO_FILE_TMP).total_seconds()
        video_duration = get_media_duration(self.VIDEO_FILE_TMP).total_seconds()

        if audio_duration and video_duration:
            if audio_duration > video_duration:
                self.cut_audio(video_duration)
                return int(video_duration), int(audio_duration)
            return int(audio_duration), int(video_duration)
        else:
            raise ValueError

    def merge_audio_video(self):
        audio_duration, video_duration = self.cut_audio_if_longer_than_video()
        subprocess.run(['ffmpeg', '-y', '-i', self.VIDEO_FILE_TMP, '-i', self.AUDIO_FILE_TMP, '-filter_complex', '[0:v]fade=t=out:st={}:d=2[v];[1:a]afade=t=out:st={}:d=2[a]'.format(video_duration-2, audio_duration-3), '-map', '[v]', '-map', '[a]', self.SLIDESHOW_FILE])

    def generate_slideshow(self):
        self.join_photo_in_video()
        self.join_audio_tracks()
        self.merge_audio_video()
        return self.SLIDESHOW_FILE
