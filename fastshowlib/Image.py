from .Size import Size

class Image:
    def __init__(self, path: str, time: int, size: Size):
        self._path = path
        self._time = time
        self.size = size

    @property
    def path(self):
        return self._path

    @property
    def time(self):
        return self._time

    @property
    def loop_parameter(self) -> str:
        return "-loop 1 -t {} -i {}".format(self.time, self.path)

    @property
    def scale(self) -> str:
        return "scale={}:{}:force_original_aspect_ratio=decrease".format(self.size.width, self.size.height)

    @property
    def pad(self) -> str:
        return "pad={}:{}:(ow-iw)/2".format(self.size.width, self.size.height)

    @property
    def setsar(self) -> str:
        return "setsar=1"

    @property
    def fade_in(self) -> str:
        return "fade=t=in:st=0:d=1"

    @property
    def fade_out(self) -> str:
        return "fade=t=out:st={}:d=1".format(self.time-1)
