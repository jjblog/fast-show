from datetime import timedelta
from re import findall
from subprocess import run

def get_media_duration(file_path) -> timedelta:
    try:
        duration = findall('[0-9][0-9]:[0-9][0-9]:[0-9][0-9]', run(['ffprobe', '-i', file_path], capture_output=True, text=True).stderr)[0].split(':')
        return timedelta(hours=int(duration[0]), minutes=int(duration[1]), seconds=int(duration[2]))
    except IndexError:
        return  None

class Music:
    def __init__(self, path: str):
        self._path = path
        self._duration = get_media_duration(path)

    @property
    def path(self):
        return self._path

    @property
    def duration(self):
        if self._duration:
            return self._duration.total_seconds()
        else:
            return None

    @property
    def fade_out(self) -> str:
        return "afade=t=out:st={}:d={}".format(self.duration, 4)

