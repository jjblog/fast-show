#! /usr/bin/python3
from fastshowlib.MediaManager import MediaManager
import subprocess


if __name__ == "__main__":
    m = MediaManager('./')
    slideshow_file = m.generate_slideshow()
    subprocess.run(['open', slideshow_file])
