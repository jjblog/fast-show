# Fast-Show

## ffmpeg
### From images to video
```bash
ffmpeg \
-loop 1 -t ${show_time} -i image1.jpeg \
-loop 1 -t ${show_time} -i image2.jpeg \
-filter_complex \
"[0:v]scale=${image_width}:${image_height}:force_original_aspect_ratio=decrease,pad=${image_width}:${image_height}:(ow-iw)/2,setsar=1,fade=t=in:st=0:d=${fade_duration}[v0];
[1:v]scale=${image_width}:${image_height}:force_original_aspect_ratio=decrease,pad=${image_width}:${image_height}:(ow-iw)/2,setsar=1,fade=t=in:st=0:d=${fade_duration},fade=t=out:st${image_duration}:d=${fade_duration}[v1];
[v0][v1]concat=n=2:v=1:a=0;
format=yuv420p[v]" \
-map "[v]" \
slide_show_no_music.mp4
```

- **-loop 1 -t ${show_time} -i image1.jpeg**: Create a loop (1 repetition) of image1\_jpeg the will last for ${show\_time} seconds
- **-filter_complex**: Set of filter (**-vf**) separeted by a **;**.
    - **[0:v]**: **zero**th element of the inputs, in particular its **v**ideo
    - **scale=${image_width}:${image_height}:force_original_apsect\ratio=decrease**
    - **pad=${image_width}:${image_height}:(ow-iw)/2**
    - **setsar=1**
    - **fade=t=in:st=0:d=${fade_duration}**
    - **fade=t=out:st=${image\duration}:d=${fade_duration}**
    - **[v1]**: References useful for next operations
    - **[v0][v1]concat=n=2:v=1:a=0**
- **-map "[v]"**: Map the video from the filter\_complex
- **slide_show_no_music.mp4**: Output file

### Join audios
```bash
ffmpeg \
-i audio1.mp3 \
-i audio2.mp3 \
-filter_complex \
"[0:a]afade=t=out:st=${audio_duration}:d=${fade_duration}[a0];
[1:a]afade=t=out:st=${audio_duration}:d=${fade_duration}[a1];
[a0][a1]concat=n=2:v=0:a=1[a]" \
-map "[a]"
audio_file.mp3
```

- **-i audio1.mp3**
- **-filter_complex**
    - **[0:a]**
    - **afade=t=out:st=${audio_duration}:d=${fade_duration}**
    - **[a0]**
    - **[a0][a1]concat=n=2:v=0:a=1[a]**
- **-map "[a]"**
- **audio_file.mp3**

### Merge video and audio
#### Cut audio if too long
```bash
ffmpeg \
-i audio_file.mp3 \
-ss 0 \
-to ${video_duration} \
-c copy \
audio_file_cutted.mp3
```

#### Merge
```bash
ffmpeg \
-i slideshow_no_music.mp4 \
-i audio_file_cutted.mp3 \
-filter_complex \
"[0:v]fade=t=out:st=${video_duration}:d=${last_fade_duration}[v];
[1:a]afade=t=out:st=${audio_duration}:d=${last_fade_duration}[a]" \
-map "[v]" \
-map "[a]" \
slideshow.mp4

```
